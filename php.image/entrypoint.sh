#!/bin/sh

cat > /etc/ssmtp/ssmtp.conf <<END
mailhub=${SMTP_HOST}:${SMTP_PORT}
UseTLS=${SMTP_USE_TLS}
UseSTARTTLS=${SMTP_USE_STARTTLS}
root=${SMTP_USER}@${SMTP_DOMAIN}
rewriteDomain=${SMTP_DOMAIN}
AuthMethod=${SMTP_AUTH}
FromLineOverride=YES
END
if [ ${SMTP_PASS:-"nope"} != "nope" ];then
	cat >> /etc/ssmtp/ssmtp.conf <<END
AuthUser=${SMTP_USER}@${SMTP_DOMAIN}
AuthPass=${SMTP_PASS}
END
fi
echo "root:${SMTP_USER}@${SMTP_DOMAIN}">/etc/ssmtp/revaliases
echo "www-data:${SMTP_USER}@${SMTP_DOMAIN}">>/etc/ssmtp/revaliases

chown -R www-data:www-data /var/www/html/store

