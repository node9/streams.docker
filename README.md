Simple Docker container for Streams
-----------------------------------

Streams is public domain communications server - ActivityPub and Nomad (the underground fediverse)

https://codeberg.org/streams/streams


Configuring and running
-----------------------

- git clone this repo to your Docker runtime directory
- git clone Streams repo to /streams subdirectory
- set DB name and password in /docker-compose.yml
- set data directories for PostgreSQL and Streams store in /docker-compose.yml
- set system email account in /php.image/ssmtp.conf
- set your site domain in /nginx.conf (fastcgi_param SERVER_NAME)
- build and run with "docker-compose up -d"
- Use /shell.sh to go to your running Streams container shell and run composer (included in image)

Allways follow Streams installation instructions (/streams/install/INSTALL.txt) and let setup create /streams/.htconfig.php\
This doc is just for container handling.

Features
--------

PHP image base: php:8.2-fpm-alpine\
PostgreSQL as recommended DB engine\
redis-alpine\
nginx-alpine

Preset for 20 concurrent connections, 1000 MB upload size, images resolution 16k


Hardware requirements
---------------------

amd64 or ARM architecture tested, 4GB RAM


PHP FPM settings
----------------

In /php.config/php-fpm.d/www.conf

Increase these variables to your site traffic and concurrent requests:\
pm.max_children = 20\
pm.start_servers = 8\
pm.min_spare_servers = 4\
pm.max_spare_servers = 12


Upload limit settings
---------------------

In /php.config/php/conf.d/streams.ini\
In /nginx.conf (client_max_body_size)\
In /imagick-policy.xml in <policymap> set limits for image upload, like width and height and other:

    <policy domain="resource" name="width" value="16KP"/>
    <policy domain="resource" name="height" value="16KP"/> 


After Install
-------------

In Site admin / Site / Advanced set "/usr/bin/convert" in Path to ImageMagick convert program\
In Site admin / Logs set "store/log" in Logfile


What is not included: proxy
---------------------------

SSL termination of https connection on main proxy (not included in docker-compose beuse in dockerized environment you probably use one)

Example HAproxy config frontend and backend:

```

frontend sslterm_443

    # here you probably put your public IP and at the end of the line path to your domain SSL certs
    bind 200.20.20.1:443 accept-proxy ssl crt /etc/haproxy/certs/
    mode http

    http-request set-header X-Forwarded-Proto https
    http-request add-header X-Forwarded-For %[src]

    # Streams Docker backend
    use_backend streams_8899  if { hdr(Host) -i yourstreamssite.lol }

    # default backend for all other sites
    default_backend apache_8080

# Streams backend (here you use your local IP of your Docker server)
backend streams_8899
    mode http
    server nginx_docker 10.10.10.100:8899 check maxconn 20

# other backend
backend apache_8080
    mode http
    server apache 10.10.10.100:8080 check maxconn 20

```

Do not forget to set proxy IP: in /nginx.conf change this line to your proxy IP

```
set_real_ip_from 10.10.10.1;
```

Use extra_hosts clause in /docker-compose.yml to set local DNS records in the containers to your SSL terminating proxy IP


EOF


